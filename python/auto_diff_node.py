
from __future__ import annotations
from typing import TYPE_CHECKING
import math
if TYPE_CHECKING:
    from auto_diff_operators import AutoDiffOperator
    from auto_diff_graph import AutoDiffGraph


class AutoDiffNode:
    def __init__(
        self,
        graph: AutoDiffGraph,
        value: float = 0,
        parents: list[AutoDiffNode] = None,
        operator: AutoDiffOperator = None,
        children: list[AutoDiffNode] = None,
        gradiant_with_respect_to_parents: list[float] = None,
        partial_derivative: float = 0
    ):
        self.graph = graph
        self.value = value
        self.parents = parents or []
        self.children = children or []
        self.operator = operator
        self.gradiant_with_respect_to_parents = gradiant_with_respect_to_parents or []
        self.partial_derivative = partial_derivative

    def __add__(self, other: AutoDiffNode):
        return self.graph.add(self, other)

    def __sub__(self, other: AutoDiffNode):
        return self.graph.sub(self, other)

    def __mul__(self, other: AutoDiffNode):
        return self.graph.mul(self, other)

    def __truediv__(self, other: AutoDiffNode):
        return self.graph.div(self, other)

    def __pow__(self, other: AutoDiffNode):
        return self.graph.pow(self, other)

    def __log__(self, other: AutoDiffNode = None):
        return self.graph.log(self, other)
