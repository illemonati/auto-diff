from __future__ import annotations

from auto_diff_node import *
from auto_diff_operators import *


class AutoDiffGraph:
    AUTO_DIFF_OPS = {
        "add": AddOperator(),
        "sub": SubOperator(),
        "mul": MulOperator(),
        "div": DivOperator(),
        "pow": PowerOperator(),
        "log": LogOperator()
    }

    def __init__(self):
        self.nodes = []

    # this orders the graph from a root node
    def generate_topological_order(self, root) -> list[AutoDiffNode]:
        order = []
        visited = set()

        # print(root, root.children)

        def add_children_dfs(node: AutoDiffNode):
            if node not in visited:
                visited.add(node)
                for child in node.children:
                    add_children_dfs(child)
                order.append(node)

        add_children_dfs(root)
        # print(root, root.children)
        # print(len(root.children))
        return order[::-1]

    def variable(self, value):
        node = AutoDiffNode(self, value)
        self.nodes.append(node)
        return node

    # this computes the partial derivative of the graph with respect to the root node
    def forward(self, root: AutoDiffNode):
        root.partial_derivative = 1
        order = self.generate_topological_order(root)
        for node in order[1:]:
            partial_derivative = 0
            for i in range(len(node.parents)):
                dnode_dparent = node.gradiant_with_respect_to_parents[i]
                dparent_droot = node.parents[i].partial_derivative
                partial_derivative += dnode_dparent * dparent_droot
            node.partial_derivative = partial_derivative
        return order[-1].partial_derivative

    def operate(self, a: AutoDiffNode, b: AutoDiffNode, operator_name: str):
        if operator_name not in AutoDiffGraph.AUTO_DIFF_OPS:
            raise RuntimeError("Invalid Operation")
        return AutoDiffGraph.AUTO_DIFF_OPS[operator_name].forward(a, b)

    def add(self, a: AutoDiffNode, b: AutoDiffNode):
        return self.operate(a, b, "add")

    def sub(self, a: AutoDiffNode, b: AutoDiffNode):
        return self.operate(a, b, "sub")

    def mul(self, a: AutoDiffNode, b: AutoDiffNode):
        return self.operate(a, b, "mul")

    def div(self, a: AutoDiffNode, b: AutoDiffNode):
        return self.operate(a, b, "div")

    def pow(self, a: AutoDiffNode, b: AutoDiffNode):
        return self.operate(a, b, "pow")

    def log(self, a: AutoDiffNode, b: AutoDiffNode = None):
        return self.operate(a, b, "log")
