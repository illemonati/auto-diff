from auto_diff import *
import math
import numpy as np
import matplotlib.pyplot as plt


def test_1():
    g = AutoDiffGraph()
    x1 = g.variable(3)
    x2 = g.variable(5)

    y = x1 + x2

    print("test 1")
    print("y:", y.value, "expected:", 3 + 5)

    dy_dx1 = g.forward(x1)
    print("dy/dx1:", dy_dx1, "expected:", 1)


def test_2():
    g = AutoDiffGraph()
    x = g.variable(12)

    a = g.variable(3)
    b = g.variable(4)
    c = g.variable(5)
    d = g.variable(6)
    f = g.variable(7)

    k = g.variable(3)
    j = g.variable(2)

    # y = 3x^3 + 4x^2 + log(5x) + 6/7
    y = a*(x**k) + b*(x**j) + g.log(c*x) + d/f

    def expected_derivative(x):
        return 9*x**2 + 8 * x + 1/x

    print("test 2")
    print("y:", y.value, "expected:", 3*(12**3) +
          4*(12**2) + math.log(5*(12)) + 6/7)

    dy_dx = g.forward(x)

    print("dy_dx:", dy_dx, "expected:", expected_derivative(x.value))


def func(x_val, y_val, diffvar):
    g = AutoDiffGraph()
    x = g.variable(x_val)
    y = g.variable(y_val)
    r = g.variable(10)*x ** (g.variable(2)*y / g.variable(1.3)) + \
        g.variable(12) ** x**y - \
        g.variable(15)**(x**(g.variable(-2) *
                             g.log(x ** g.variable(2))/(y**g.variable(2)**x)))

    dr_ddiffvar = g.forward(x if diffvar == 'x' else y)

    return r.value, dr_ddiffvar


def plot_test():
    print("plot test")
    xs = np.linspace(0.001, 1.5, 5000).tolist()
    r1s = [func(x, 10, 'x') for x in xs]
    r2s = [func(1.2, x, 'y') for x in xs]
    plt.plot(xs, r1s)
    plt.plot(xs, r2s)
    ax = plt.gca()
    ax.set_xlim([0, 1.5])
    ax.set_ylim([-40, 50])
    plt.savefig("plot.png")
    plt.show()


def test():
    # test_1()
    # test_2()
    plot_test()


if __name__ == '__main__':
    test()
