from __future__ import annotations

from auto_diff_node import AutoDiffNode
from abc import abstractmethod
import math


class AutoDiffOperator:

    name = "operator"

    @abstractmethod
    def forward(self, a: AutoDiffNode, b: AutoDiffNode):
        pass

    def forward_helper(
        self,
        a: AutoDiffNode,
        b: AutoDiffNode,
        value: float,
        operator: AutoDiffOperator,
        gradiant_with_respect_to_parents: list[float]
    ):
        if a.graph != b.graph:
            raise RuntimeError("Nodes must be on same graph")

        parents = [a, b]
        result = AutoDiffNode(a.graph, value, parents, operator)
        result.gradiant_with_respect_to_parents = gradiant_with_respect_to_parents
        a.children.append(result)
        b.children.append(result)
        return result


class LogOperator(AutoDiffOperator):
    def __init__(self):
        self.name = "log"

    def forward(self, a: AutoDiffNode, base: AutoDiffNode = None):
        if base == None:
            base = AutoDiffNode(a.graph, math.e)

        # log_b(a) = log(a)/log(b)
        la = math.log(a.value)
        lbase = math.log(base.value)

        value = la/lbase

        # d/da log_b(a) = 1/(alog(b))
        da = 1/(a.value * lbase)

        # d/db log_b(a) = -log(a) / (blog^2(b))
        db = - la / (base.value*(lbase**2))

        return self.forward_helper(
            a,
            base,
            value,
            operator=self,
            gradiant_with_respect_to_parents=[da, db]
        )


class MulOperator(AutoDiffOperator):
    def __init__(self):
        self.name = "mul"

    def forward(self, a: AutoDiffNode, b: AutoDiffNode):
        value = a.value * b.value
        da = b.value
        db = a.value
        return self.forward_helper(
            a, b,
            value,
            operator=self,
            gradiant_with_respect_to_parents=[da, db]
        )


class DivOperator(AutoDiffOperator):
    def __init__(self):
        self.name = "div"

    def forward(self, a: AutoDiffNode, b: AutoDiffNode):
        value = a.value / b.value
        da = 1 / b.value
        db = -value / b.value
        return self.forward_helper(
            a, b,
            value,
            operator=self,
            gradiant_with_respect_to_parents=[da, db]
        )


class AddOperator(AutoDiffOperator):
    def __init__(self):
        self.name = "add"

    def forward(self, a: AutoDiffNode, b: AutoDiffNode):
        value = a.value + b.value
        da = 1
        db = 1
        return self.forward_helper(
            a, b,
            value,
            operator=self,
            gradiant_with_respect_to_parents=[da, db]
        )


class SubOperator(AutoDiffOperator):
    def __init__(self):
        self.name = "sub"

    def forward(self, a: AutoDiffNode, b: AutoDiffNode):
        value = a.value - b.value
        da = 1
        db = -1
        return self.forward_helper(
            a, b,
            value,
            operator=self,
            gradiant_with_respect_to_parents=[da, db]
        )


class PowerOperator(AutoDiffOperator):
    def __init__(self):
        self.name = "pow"

    def forward(self, a: AutoDiffNode, b: AutoDiffNode):
        a_b1 = (a.value)**(b.value-1)
        # a**b == a**(b-1) * a
        value = a_b1 * a.value
        # power rule
        da = b.value * a_b1
        # d/db a**b = (a**b)log(a)
        db = value * math.log(a.value)
        return self.forward_helper(
            a, b,
            value,
            operator=self,
            gradiant_with_respect_to_parents=[da, db]
        )
